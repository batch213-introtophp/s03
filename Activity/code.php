<?php

$personObj = (object)[
	'firstName' => 'Taehyung',
	'middleName' => ' ',
	'lastName' => 'Kim'
];

class Person {
	public $firstName;
	public $middleName;
	public $lastName;

	public function __construct($lastName, $middleName, $firstName)
	{
		$this->firstName = $firstName;
		$this->middleName = $middleName;
		$this->lastName = $lastName;
	}
	public function printName(){
		return "Your fullname is $this->lastName $this->middleName $this->firstName";
	}
}

class Developer extends Person{

	public function printName(){
		return "Your name is $this->lastName $this->middleName $this->firstName and you are developer.";
	}
}

class Engineer extends Person{

	public function printName(){
		return "You are an engineer named $this->lastName $this->middleName $this->firstName.";
	}
}

$person = new Person('Kim',' ','Taehyung');
$developer = new Developer('Min',' ','Yoongi');
$engineer = new Engineer('Park',' ','Jimin');
