<?php require_once './code.php' ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S03 Activity</title>
</head>
<body>
	<h1>PERSON</h1>
	<p><?php echo $person->printName(); ?></p>

	<h1>DEVELOPER</h1>
	<p><?php echo $developer->printName(); ?></p>

	<h1>ENGINEER</h1>
	<p><?php echo $engineer->printName(); ?></p>

</body>
</html>